use DBI;
use Getopt::Long;

my ($help, $host);

GetOptions(
    "help" => \$help,
    "host=s" => \$host
);

if (defined $help) {
    print "This script will output a csv file with the links to reconfirm each email address.
It looks for a reconfirm.db file in the current working directory.\n";
    exit 0;
}

die 'Please provide --host option. Host should be the hostname hosting the mailman-reconfirm service' unless defined $host;

my $dbh = DBI->connect('dbi:SQLite:reconfirm.db', '', '') or die "Could not connect to the database\n";

my $stmt = qq(SELECT email,uuid from reconfirm;);
my $sth = $dbh->prepare($stmt);
my $rv = $sth->execute() or die $DBI::errstr;

open my $fh, '>', 'links.csv';
print $fh "Email,Link\n";

while(my @row = $sth->fetchrow_array()) {
    print $fh $row[0] . ',' . create_link($host, $row[1]) . "\n";
}

sub create_link {
    my ($host, $uuid) = @_;

    return "https://$host/reconfirm?ID=$uuid";
}
