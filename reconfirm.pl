#!/usr/bin/env perl

use DBI;
use Mojolicious::Lite;
use UUID 'uuid';

my $dbh = DBI->connect('dbi:SQLite:data/reconfirm.db', '', '') or die "Could not connect to the database\n";

# Generate random UUID for every row with missing UUID
my $stmt = qq(SELECT email from reconfirm where uuid is null;);
my $sth = $dbh->prepare($stmt);
my $rv = $sth->execute() or die $DBI::errstr;

app->log->info('Populating missing UUIDs');
while(my @row = $sth->fetchrow_array()) {
    my $email = $row[0];
    my $uuid = uuid();
     app->log->info("Generating UUID for user $email");
    $stmt = qq(UPDATE reconfirm set uuid='$uuid' where email='$email';);
    my $rv = $dbh->do($stmt) or die $DBI::errstr;
}

app->log->info('Populating missing UUIDs... done');

get '/reconfirm' => sub {
    my $self = shift;
    my $userID = $self->req->query_params->param('ID');
    $self->render(
        ID       => $userID,
        template => 'reconfirm'
    );
};

post '/reconfirm' => sub {
    my $self = shift;
    my $userID = $self->req->param('ID');

    # Test if the UUID exists
    $stmt = qq(select email from reconfirm where uuid=?;);
    my @row = $dbh->selectrow_array($stmt, undef, $userID);

    # Change the defauilt Server HTTP Header
    $self->res->headers->server('Custom');

    unless (@row) {
        $self->res->code(500);
        return $self->reply->exception('Failed to find user ID ' . $userID);
    }

    my ($email) = @row;
    # If the user exists, set confirmed=1 in the DB
    my $stmt = $dbh->prepare(qq(UPDATE reconfirm set confirmed=1 where uuid=?;));
    $rv = $stmt->execute($userID) or warn $DBI::errstr;
    $self->app->log->info("User $email confirmed");
    $self->render(
        text => "User $email confirmed! Thank you!"
    );
};

# Catch every other endpoint
any '/(*)' => sub {
    my $self = shift;
    $self->res->code(404);
    $self->res->headers->server('Custom');
    $self->res->message('Not Found');

    $self->render('not_found');
};

app->start;

__DATA__
@@ reconfirm.html.ep
<!DOCTYPE html>
<html>
<head><title>List membership reconfirmation</title></head>
<body>
Please click on the "Re-confirm" button below to indicate you want to continue
being part of the mailing list.
</br>
<form method="POST" action="/reconfirm">
<input name="ID" value="<%= $ID %>">
<input type="submit" value="Re-confirm">
</form>
</html>

@@ not_found.html.ep
<!DOCTYPE html>
<html>
<head><title>Page not found</title></head>
<body>There is nothing here</body>
</html>

@@ exception.html.ep
<!DOCTYPE html>
<html>
<head><title>Error</title></head>
<body>An error occured</body>
</html>
