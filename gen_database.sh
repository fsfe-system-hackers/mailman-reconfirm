#!/bin/bash

# Test whether necessary applications are installed
function testinst {
  APP=$1
  if [ $(which $APP | wc -l) != 1 ]; then
    echo "$APP does not seem to be installed. Aborting."
    exit 1
  fi
}
testinst sqlite3
testinst perl

# Help listing
if [ "$1" = "" ]; then
  self=$(basename $0) 
  echo "No parameters or variables given!"
  echo
  echo "Usage: "
  echo
  echo "$self MAIL_FILE DATABASE"
  echo "   -- Populate a list of email addresses to the given database."
  echo "      The list can be a plain text file with one email per line."
  echo "      The database is supposed to be a Sqlite database."
  echo
  echo "$self auto-deploy"
  echo "   -- Automatically creates a database if it doesn't exist."
  echo "      Meant to be started by Docker for automatic deployment."
  echo
  echo "This script allows you to (re-)create a new database, fill it with data, "
  echo "and generate the UUIDs."
  exit 0
fi

LIST="$1"
DB="$2"

# Automatic creation of database if it doesn't exist
if [[ "$1" == "auto-deploy" ]]; then
  if [[ ! -e "data/reconfirm.db" ]]; then
    touch data/reconfirm.db
    sqlite3 data/reconfirm.db "create table reconfirm (id INT PRIMARY KEY, email TEXT NOT NULL, uuid TEXT, confirmed INTEGER default 0);"
    echo "Created empty database data/reconfirm.db since it didn't exist yet."
    exit 0
  else
    echo "Database data/reconfirm.db already exists."
    exit 0
  fi
fi

# Create database
if [[ -e "${DB}" ]]; then
  while true; do
    read -p "Database ${DB} already exists. Do you want to delete and re-create it? [y/n]: " yn
    case $yn in
      [Yy]* ) rm "${DB}"; \
              touch "${DB}"; \
              sqlite3 "${DB}" "create table reconfirm (id INT PRIMARY KEY, email TEXT NOT NULL, uuid TEXT, confirmed INTEGER default 0);"; \
              echo "New database created."; break;;
      [Nn]* ) echo "Using existing database."; break;;
      * ) echo "Wrong input. y or n?";;
    esac
  done
else
  touch "${DB}"
  sqlite3 "${DB}" "create table reconfirm (id INT PRIMARY KEY, email TEXT NOT NULL, uuid TEXT, confirmed INTEGER default 0);"
  echo "New database created."
fi

# Import emails
while true; do
  read -p "Fill ${DB} with emails from ${LIST}? [y/n]: " yn
  case $yn in
    [Yy]* ) while read line; do \
              sqlite3 "${DB}" "INSERT INTO reconfirm ('email') values ('$line');"; \
            done < "${LIST}"; \
            echo "Database populated with emails."; break;;
    [Nn]* ) echo "Database not populated with emails."; break;;
    * ) echo "Wrong input. y or n?";;
  esac
done

# Generate UUIDs
while true; do
  read -p "Generate an UUID for each entry in ${DB}? [y/n]: " yn
  case $yn in
    [Yy]* ) if [[ -e "reconfirm.pl" ]]; then perl reconfirm.pl && echo "UUIDs created."; else echo "[ERROR] reconfirm.pl script not found in this directory."; exit 1; fi; \
            break;;
    [Nn]* ) echo "No UUIDs created"; break;;
    * ) echo "Wrong input. y or n?";;
  esac
done

echo "All done. Now enter the data directors and execute 'perl ../gen_links.pl --host <tool hostname>' to generate the CSV with all mails and links." 
