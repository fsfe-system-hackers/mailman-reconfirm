# Mailman reconfirmation tool

The goal of this tool is to be able to ask users to reconfirm they want to be
part of a mailing list. We do that by sending a email to each member of the list,
asking to click on a link if they're still interested in being a member of the
list.

## Requirements

### SQLite database

We create a sqlite table `reconfirm.db` with the following columns:

- email
- UUID
- confirmed, defaults to 0

With this command:

```
create table reconfirm (id INT PRIMARY KEY, email TEXT NOT NULL, uuid TEXT, confirmed INTEGER default 0);
```

### Dependencies

On debian run the following commands to install the script's dependencies:

```
apt-get install uuid-dev sqlite3
cpan install DBI Mojolicious DBD::SQLite UUID
```

### Usage

Run the script:

```
perl reconfirm.pl daemon -m production -l http://[::]:3000
```

The program will create random UUID for each row in the sqlite database. We can
after that create links to send to the mailing list members that look like this:

```
https://<host>:8000/reconfirm?ID=UUID
```

### Generating confirmation links

Once the SQLite database is populated and the script has been running, generate
the links with the script [gen_links.pl](gen_links.pl). Put it in the same
directory as the database, and then run it with:

```
perl gen_links.pl --host <hostname hosting the reconfirm-mailman service>
```

This will generate a file called `links.csv` with the association email <=>
link. The links can be sent to the mailing list members.

### Docker

Is it also possible to use the Dcokerfile directly. For this run:

```
docker build -t reconfirm-mailman https://git.fsfe.org/fsfe-system-hackers/mailman-reconfirm.git
docker run -v /path/to/sqlitereconfirm_db:/home/perlscript/mailman-reconfirm --name mailman-reconfirm -p 3000 reconfirm-mailman
```

### Ansible

A playbook can be used to deploy the container remotely.

```
ansible-playbook playbook.yml -i lund.fsfeurope.org
```

## Usage on the FSFE's Docker deployment

If deployed via the FSFE's Drone/Ansible/Docker toolchain, the usage of
this tool will look like the following:

1. Create a list of the email addresses to be confirmed in
   `/srv/mailman-reconfirm/data/`, e.g. called `mails.txt`. It has to be
   one email per line.
2. Enter the docker container in which all necessary tools are already
   installed by `docker exec -it mailman-reconfirm bash`
3. In `~/mailman-reconfirm`, execute the helper script with
   `./gen_database.sh data/mails.txt data/reconfirm.db`. The script asks
   for each step. For a complete re-creation of the database, always
   enter `y`. Make sure you saved the old database to a safe place if it
   is still needed for another operation.
4. Enter the `data` subdirectory and execute `perl ../gen_links.pl --host mailman-confirm.fsfe.org` to create a file called `links.csv`.

The `links.csv` file is a comma-separated list of mail addresses with
the confirmation links. You can use tools like the Thunderbird addon
"Mail Merge" to auto-generate emails for each address with the
respective link.

## License

This software is copyright 2018 by the Free Software Foundation Europe e.V. and
licensed under the GPLv3 license. For details see the "LICENSE" file in the top
level directory of https://git.fsfe.org/fsfe-system-hackers/mailman-reconfirm
